package cyc.challenge.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CocheControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getCoche(){
        ResponseEntity<String> response = restTemplate.getForEntity("/coche/1", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);
        assertEquals(getCocheResponse(), response.getBody());
    }

    @Test
    public void getCoches(){
        ResponseEntity<String> response = restTemplate.getForEntity("/coche", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);
        assertEquals(getCochesResponse(), response.getBody());
    }

    @Test
    public void getCochesOrderBy(){
        ResponseEntity<String> response = restTemplate.getForEntity("/coche?orderBy=fechaVenta", String.class);
        System.out.println("--------GETfechaVenta: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);
        assertEquals(getCochesOrderByVentas(), response.getBody());

        ResponseEntity<String> response2 = restTemplate.getForEntity("/coche?orderBy=fechaIngreso", String.class);
        System.out.println("--------GETfechaIngreso: "+ response2.getBody());
        assert(response2.getStatusCode()).equals(HttpStatus.OK);
        assertEquals(getCochesOrderByIngreso(), response2.getBody());
    }

    private String getCocheResponse(){
        return "{\"id\":1," +
                "\"marca\":\"Ferrar\"," +
                "\"coste\":66666.99," +
                "\"fechaIngreso\":\"2020-11-01T05:12:24.000+00:00\"," +
                "\"fechaVenta\":\"2020-11-13T05:12:24.000+00:00\"," +
                "\"vendido\":true," +
                "\"matricula\":\"MAT123RI\"," +
                "\"precioVenta\":99999.99}";
    }

    private String getCochesResponse(){
        return "[{\"id\":1,\"marca\":\"Ferrar\",\"coste\":66666.99,\"fechaIngreso\":\"2020-11-01T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-13T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"MAT123RI\",\"precioVenta\":99999.99}," +
                "{\"id\":2,\"marca\":\"Audi\",\"coste\":65432.99,\"fechaIngreso\":\"2020-11-02T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-12T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"AUD123I0\",\"precioVenta\":88888.99}]";
    }

    private String getCochesOrderByVentas(){
        return "[{\"id\":1,\"marca\":\"Ferrar\",\"coste\":66666.99,\"fechaIngreso\":\"2020-11-01T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-13T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"MAT123RI\",\"precioVenta\":99999.99}," +
                "{\"id\":2,\"marca\":\"Audi\",\"coste\":65432.99,\"fechaIngreso\":\"2020-11-02T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-12T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"AUD123I0\",\"precioVenta\":88888.99}]";
    }

    private String getCochesOrderByIngreso(){
        return "[{\"id\":2,\"marca\":\"Audi\",\"coste\":65432.99,\"fechaIngreso\":\"2020-11-02T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-12T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"AUD123I0\",\"precioVenta\":88888.99}," +
                "{\"id\":1,\"marca\":\"Ferrar\",\"coste\":66666.99,\"fechaIngreso\":\"2020-11-01T05:12:24.000+00:00\",\"fechaVenta\":\"2020-11-13T05:12:24.000+00:00\",\"vendido\":true,\"matricula\":\"MAT123RI\",\"precioVenta\":99999.99}]";    }
}
