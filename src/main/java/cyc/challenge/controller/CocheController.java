package cyc.challenge.controller;

import com.querydsl.core.types.Predicate;
import cyc.challenge.entity.Coche;
import cyc.challenge.service.CocheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class CocheController {

    @Autowired
    private CocheService service;

    @GetMapping("/coche/{id}")
    public Coche getById(@PathVariable (value="id") Integer id){
        return service.findById(id);
    }

    @GetMapping("/coche")
    public List<Coche> getAll(@RequestParam (required=false) String orderBy){
        return service.findAll(orderBy);
    }

   /* @PostMapping("/coche")
    public Coche createCoche(@RequestBody Coche coche) {
        return service.save(coche);
    }*/
}