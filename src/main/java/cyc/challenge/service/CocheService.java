package cyc.challenge.service;

import com.querydsl.core.types.Predicate;
import cyc.challenge.entity.Coche;
import cyc.challenge.repository.CocheRepository;
import cyc.challenge.repository.CocheRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CocheService {

    @Autowired
    private CocheRepository repository;

    @Autowired
    private CocheRepositoryImpl repositoryImpl;

    public Coche findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    public List<Coche> findAll() {
        return repository.findAll();
    }

    public List<Coche> findAllOrderByFechaVenta() {
        return repositoryImpl.findAllOrderByFechaVenta();
    }

    public List<Coche> findAllOrderByFechaIngreso() {
        return repositoryImpl.findAllOrderByFechaIngreso();
    }

    public List<Coche> findAll(String orderBy) {
        if(orderBy!=null){
            if(orderBy.equals("fechaVenta"))
                return findAllOrderByFechaVenta();
            if(orderBy.equals("fechaIngreso"))
                return findAllOrderByFechaIngreso();
        }
        return findAll();
    }
}
