package cyc.challenge.repository;

import com.querydsl.core.types.Predicate;
import cyc.challenge.entity.Coche;
import cyc.challenge.entity.QCoche;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

public class CocheRepositoryImpl extends QuerydslRepositorySupport {

    private static final QCoche qCoche = QCoche.coche;

    public CocheRepositoryImpl() {
        super(Coche.class);
    }

    public List<Coche> findAllOrderByFechaVenta(){
        return from(qCoche).orderBy(qCoche.fechaVenta.desc()).select(qCoche).fetch();
    }

    public List<Coche> findAllOrderByFechaIngreso(){
        return from(qCoche).orderBy(qCoche.fechaIngreso.desc()).select(qCoche).fetch();
    }
}
