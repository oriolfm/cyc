package cyc.challenge.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "concesionario")
public class Concesionario {

    @Id
    private Integer id;

    private String direccion;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "coches_concecionario",
            joinColumns = @JoinColumn(name = "concesionario_id"),
            inverseJoinColumns = @JoinColumn(name = "coche_id"))
    private Set<Coche> coches;

}