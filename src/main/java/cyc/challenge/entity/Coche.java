package cyc.challenge.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "coche")
public class Coche {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String marca;

    private Float coste;

    private Timestamp fechaIngreso;

    private  Timestamp fechaVenta;

    private  boolean vendido;

    private String matricula;

    private Float precioVenta;

}